#!/bin/bash

testconn() {

  if [ -z "$1" ]; then 
    echo "Usage: testconn <ip> <port> [timeout=2]"
    kill -INT $$
  fi

  if [ -z "$3" ]; then
    timeout="2"
  else
    timeout=${3}
  fi

    while : ; do
      echo -n "Attempting connection..."
      echo "" | nc -w $timeout $1 $2 > /dev/null 2>&1
      retval="${?}"
      if [ "${retval}" == 1 ]; then
        printf "\033[0;31mfailure\n\033[0m"
      else
        printf "\033[0;32msuccess\n\033[0m"
      fi
      sleep $timeout
    done
}

sshclient() {

  if [ -z "$1" ]; then 
    echo "Usage: sshclient <username@host> [port=22]"
    kill -INT $$
  fi

  if [ -z "$2" ]; then
    port="22"
  else
    port=${2}
  fi

  ssh -p $port $1
}

get_os_term() {
  if [ $(uname) == 'Darwin' ] && [ "${TERM_PROGRAM}" != 'iTerm.app' ]; then
    os_term="Darwin_Terminal"
  elif [ "${TERM_PROGRAM}" == 'iTerm.app' ]; then
    os_term="Darwin_iTerm"
  elif [ "${TERM}" == 'xterm-256color' ] || [ "${TERM}" == 'linux' ]; then
    os_term="Linux_xterm"
  fi
}

copy() {
  echo "copying \"$1\" to \"$2\""
  cp -f $1 $2
  echo ""
}

colorgrid() {

  cat "$0" 1>&2;

  Colors="$(
# Colors (0..15):
      i=0;

      while
      echo "$i";
      [ $i -lt 15 ];

      do
      i=$(( $i + 1 ));

      done;
      )";


  echo;

  for x0 in '48' '38'; # Background / Foreground

    do {
      for Color in $Colors;

        do {
          printf '\e['"$x0"';5;%sm  %3s  ' "$Color" "$Color"; # [Note 1]

# 8 entries per line:
          [ $(( ($Color + 1) % 8 )) -eq 0 ] && echo -e '\e[m'; # [Note 1]
        };

        done;
      };

  done;

  unset Colors x0;
  echo;


# === Table 16..255 ===

  for Color in \
    $(
# Colors (16..255):
    i=16;

    while
      echo "$i";
      [ $i -lt 255 ];

      do
        i=$(( $i + 1 ));

        done;
     );


  do {
    Colors="$Colors $Color";

# 6 entries per group:
    [ $(( ($Color + 1) % 6 )) -eq 4 ] && {
      for x0 in \
        '38' '48'; # Foreground / Background

        do {
          for Color in \
          $Colors;

          do
            printf '\e['"$x0"';5;%sm  %3s  ' "$Color" "$Color"; # [Note 1]
          done;

          echo -ne '\e[m'; # [Note 1]
        };

      done;

      unset Colors x0;
      echo;
    };

  };


  done;

  unset Color;
  echo;
}

alias ssh2console=sshclient

#!/bin/bash

vi_src_dir="${HOME}/shell-cfg/vi"
macos_src_dir="${HOME}/shell-cfg/macos"
bash_src_dir="${HOME}/shell-cfg/bash"
functions_src_dir="${HOME}/shell-cfg/functions"
ssh_src_dir="${HOME}/shell-cfg/ssh"
git_src_dir="${HOME}/shell-cfg/git"
vimdir="${HOME}/.vim"
gitconfig="${HOME}/.gitconfig"
cd `dirname $0`

source ${functions_src_dir}/functions.sh

if [ ! -d "${vimdir}" ] ; then
	echo "${vimdir} doesn't exist, creating..."
	mkdir -p ${vimdir}/colors
	mkdir -p ${vimdir}/backups
elif [ ! -d "${vimdir}/backups" ] ; then
	echo "${vimdir}/backups doesn't exist, creating..."
	mkdir ${vimdir}/backups
elif [ ! -d "${vimdir}/colors" ] ; then
	echo "${vimdir}/colors doesn't exist, creating..."
	mkdir ${vimdir}/colors
fi
copy ${vi_src_dir}/Nord.vim ${vimdir}/colors
copy ${vi_src_dir}/bubblegum-256-dark.vim ${vimdir}/colors

if [ -f "${gitconfig}" ] ; then
	echo "gitconfig exists, using existing gitconfig"
else
	copy ${git_src_dir}/gitconfig ${HOME}/gitconfig
fi


if [ -f "${HOME}/.vimrc" ] ; then
	echo "\"${HOME}/.vimrc\" exists, making backup to \"/tmp/vimrc.${USER}\""
	cp ${HOME}/.vimrc /tmp/vimrc.${USER}
fi
copy ${vi_src_dir}/vimrc ${HOME}/.vimrc

if [ -f "${HOME}/.bashrc" ] ; then
	echo "\"${HOME}/.bashrc\" already exists, making backup to \"/tmp/bashrc.${USER}\""
	cp ${HOME}/.bashrc /tmp/bashrc.${USER}
fi
copy ${bash_src_dir}/bashrc ${HOME}/.bashrc
copy ${bash_src_dir}/digrc ${HOME}/.digrc
copy ${bash_src_dir}/dir_colors ${HOME}/.dir_colors

if [ ! -f "${HOME}/.bash_profile" ] ; then
	ln ${HOME}/.bashrc ${HOME}/.bash_profile
else
	echo "Potential .bashrc and .bash_profile conflict; fix manually"
fi

if [ ! -d "${HOME}/.ssh" ] ; then
	mkdir ${HOME}/.ssh
  copy ${ssh_src_dir}/config ${HOME}/.ssh
elif [ -f "${HOME}/.ssh/config" ] ; then
	echo "\"${HOME}/.ssh/config\" already exists, making backup to \"/tmp/ssh_config.${USER}\""
	copy ${HOME}/.ssh/config /tmp/ssh_config.${USER}
fi

source ${HOME}/.bashrc
echo "Done!"

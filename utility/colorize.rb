#!/usr/bin/ruby

#require 'geoip'

#geoip_db_dir="/usr/share/xt_geoip"

loghash = {}

def mkhash(str, loghash)
	key = str.split("=")[0]
  value = str.split("=")[1]
  loghash[key] = value
end

# Sep 22 11:56:52 R14RKRU21 kernel: [ 4386.209685] iptables denied: IN=eth0 OUT= MAC=6c:f0:49:a0:5c:98:00:12:f2:c1:0d:00:08:00 SRC=64.125.239.29 DST=69.30.216.196 LEN=40 TOS=0x00 PREC=0x00 TTL=245 ID=54321 PROTO=TCP SPT=59940 DPT=3128 WINDOW=65535 RES=0x00 SYN URGP=0
STDIN.each do |log|

	arr = log.split

	month = arr[0]
	day = arr[1]
	time = arr[2]
	system = arr[3]
	action = arr[7]
	
	7.upto(arr.length) do |i|
		if arr[i] =~ /=/ then
			mkhash(arr[i], loghash)
		end
	end
	
	#c = GeoIP.new("#{geoip_db_dir}/GeoIP.dat").country(loghash["SRC"]).to_hash
	#loghash["CC"] = c[:country_code2]
	#loghash["CN"] = c[:country_name]
	
	if log =~ /denied:/ then 
		loghash["action"] = "\033[31mDENIED\033[0m"
	else
		loghash["action"] = "\033[32mPERMIT\033[0m"
	end
	
	if loghash["PROTO"] == "ICMP"
		loghash["DPT"] = "-----"
		loghash["SPT"] = "-----"
	end
	
	#printf("%s %2s %s %6s %6s %15s %5s %15s %5s %4s %4s %2s %s %s\n", month, day, time,
	printf("%s %2s %s %6s %6s %15s %5s %15s %5s %4s %4s %s\n", month, day, time,
 	loghash["action"], 
 	loghash["IN"], 
 	loghash["SRC"], 
 	loghash["SPT"], 
 	loghash["DST"], 
 	loghash["DPT"], 
 	loghash["LEN"], 
 	loghash["PROTO"],
 	#loghash["CC"],
 	#loghash["CN"],
 	loghash["WANTS"])
end

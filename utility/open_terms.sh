#!/bin/bash

if [ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
	echo "Syntax: open_terms.sh <conntype> <ip> <port>"
	echo "where <conntype> is \"console\" or \"ssh\" and where <port> is the port"
	echo "number of leaf1's console (RE0) port; the rest will be calculated"
	exit 1
fi

if [ "$TERM_PROGRAM" != 'iTerm.app' ]; then
	echo "This script requires the use of the MacOS terminal program \"iTerm2\""
	exit 1
fi

#title() { echo -ne "\033]0;"$*"\007" }

# new_tab tabname ssh
new_tab() {
	osascript \
  	-e 'tell application "iTerm2" to tell current window to set newWindow to (create tab with default profile)'\
   	-e "tell application \"iTerm2\" to tell current session of newWindow to write text \"title $1\""\
   	-e "tell application \"iTerm2\" to tell current session of newWindow to write text \"${2}\""
}

conntype=$1
ip=$2
port=$3
switches="leaf1 leaf2 spine1 spine2"

# consoles
for switch in $switches; do
	#if [ "$conntype" == 'ssh' ]; then
	#	(( port=$port + 5 ))
	#	conntype=""
	#fi
	oldifs=$IFS
	IFS=$'\n'
	sshcmd="ssh jcluser@${ip} -p ${port}"
	new_tab ${switch} ${sshcmd}
	(( port=$port + 4 ))
	IFS=$oldifs
done

#ssh
#for switch in $switches; do
#	sshclient jcluser@${ip} (( ${port} + 3 ))
#	sshclient jcluser@${ip} (( ${port} + 7 ))
#	sshclient jcluser@${ip} (( ${port} + 11 ))
#	sshclient jcluser@${ip} (( ${port} + 15 ))
#done


